# Revolut task

Repository for recruitment assignment commission by Revolut

## Overview

This is an Android application project that:
* calculation currency exchange
* provides real-time data about exchange rates from Revolut database endpoint

## Architecture

The application is built on the top of MVVM pattern. The most important components are:

### [Model] Repository

RatesRepository - combine local and remote service to provide a fresh or cached date 

### [Model] Remote service

RatesService - is a Retrofit 2 interface that contains a pull method for exchange rates 

### [Model] Local service

Local service cache date in case of losing connectivity

### [ViewModel] ViewModel 

ConventerViewModel holds a state of the view. By LiveData expose exchange rates from repositor.
It also now about currently calculated currency amount

### [View] RecyclerView

To display the list I use RecyclerView as the item's set is large and change dynamically.

### [View] Activity

Using sealed class I express the state of repository and view, which help to clean handling different scenarios by "when" statement  

## Dependency Injection

Koin library was used to introduce dependency injection into the project
