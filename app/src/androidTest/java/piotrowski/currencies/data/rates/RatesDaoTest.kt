package piotrowski.currencies.data.rates

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import piotrowski.currencies.data.rates.models.RatesList
import piotrowski.currencies.data.rates.sources.local.RatesListDao
import piotrowski.currencies.data.rates.sources.local.RatesListDatabase

@RunWith(AndroidJUnit4::class)
class RatesDaoTest {

    private lateinit var db: RatesListDatabase
    private lateinit var dao: RatesListDao

    @Before
    fun init() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context,
            RatesListDatabase::class.java
        ).build()
        dao = db.ratesListDao()
    }

    @After
    fun closeDb() {
        db.close()
    }

    @Test
    fun test_that_data_are_insert_to_database() = runBlocking {
        val rateList = RatesList("EUR", mapOf("USD" to 2f, "PLN" to 3f))

        dao.insertOrUpdate(rateList)

        val result = dao.getByIsoCode("EUR")

        assert(rateList == result)
    }

    @Test
    fun test_that_data_are_update_in_database() = runBlocking {
        val rateList1 = RatesList("EUR", mapOf("USD" to 2f))

        val rateList2 = RatesList("EUR", mapOf("PLN" to 3f))

        dao.insertOrUpdate(rateList1)
        dao.insertOrUpdate(rateList2)

        val result = dao.getByIsoCode("EUR")

        assert(result == rateList2)
    }
}