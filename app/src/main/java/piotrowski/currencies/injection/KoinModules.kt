package piotrowski.currencies.injection

import androidx.room.Room
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import piotrowski.currencies.BuildConfig
import piotrowski.currencies.data.rates.RatesListRepository
import piotrowski.currencies.data.rates.sources.local.RatesListDatabase
import piotrowski.currencies.data.rates.sources.remote.RatesService
import piotrowski.currencies.ui.converter.ConverterViewModel
import retrofit2.Retrofit

@ExperimentalSerializationApi
private val NetworkDependency = module {

    //OkHttp
    single {
        OkHttpClient().newBuilder().apply {
            if (BuildConfig.DEBUG) {
                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                addInterceptor(loggingInterceptor)
            }
        }.build()
    }

    //Retrofit
    single {
        Retrofit
            .Builder()
            .client(get())
            .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
            .baseUrl(BuildConfig.BASE_URL).build()
    }

    //Rates List Service
    single {
        get<Retrofit>()
            .create(RatesService::class.java)
    }
}

private val DataDependency = module {

    //Rates database
    single {
        Room
            .databaseBuilder(androidApplication(), RatesListDatabase::class.java, "ratesDB")
            .build()
            .ratesListDao()
    }

    //Rates repository
    single { RatesListRepository(get(), get()) }
}

private val ViewModelDependency = module {
    viewModel { ConverterViewModel(get(), androidApplication()) }
}

@ExperimentalSerializationApi
val appComponent = listOf(
    NetworkDependency,
    DataDependency,
    ViewModelDependency
)


