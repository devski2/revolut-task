package piotrowski.currencies.data.rates.sources.remote

import piotrowski.currencies.data.rates.models.RatesList
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesService {

    @GET("api/android/latest")
    suspend fun getRates(@Query("base") baseName: String): RatesList
}