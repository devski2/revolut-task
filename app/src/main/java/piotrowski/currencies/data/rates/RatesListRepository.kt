package piotrowski.currencies.data.rates

import android.util.Log
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import piotrowski.currencies.data.rates.models.RatesList
import piotrowski.currencies.data.rates.sources.local.RatesListDao
import piotrowski.currencies.data.rates.sources.remote.RatesService

class RatesListRepository(
    private val ratesService: RatesService,
    private val ratesListDao: RatesListDao
) {

    suspend fun update(isoCode: String) {
        val remoteResult = ratesService.getRates(isoCode)
        ratesListDao.insertOrUpdate(remoteResult)
    }

    suspend fun getByIsoCode(isoCode: String): Flow<Result> = flow {
        val cache = ratesListDao.getByIsoCode(isoCode)

        if (cache == null) {
            emit(Result.Initializing)
        } else {
            emit(Result.Cached(cache))
        }

        try {
            update(isoCode)
            emit(Result.Success(ratesListDao.getByIsoCode(isoCode)!!))
        } catch (ex: Exception) {
            Log.e(
                "RatesListRepository",
                "Exception ${ex.javaClass.simpleName} occurs \nwith message: ${ex.message ?: "no message"}\nwhile updating data"
            )
            emit(Result.Error(ex))
        }
    }

    sealed class Result(open val data: RatesList) {
        object Initializing : Result(RatesList("", mutableMapOf()))
        data class Success(override val data: RatesList) : Result(data)
        data class Cached(override val data: RatesList) : Result(data)
        data class Error(val exception: Exception) : Result(RatesList("", mutableMapOf()))
    }
}