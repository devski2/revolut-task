package piotrowski.currencies.data.rates.sources.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import piotrowski.currencies.data.rates.models.RatesList

@Database(entities = [(RatesList::class)], version = 1)
@TypeConverters(MapConverter::class)
abstract class RatesListDatabase : RoomDatabase() {
    abstract fun ratesListDao(): RatesListDao
}

class MapConverter {
    @TypeConverter
    fun fromString(value: String): Map<String, Float> = Json.decodeFromString(value)

    @TypeConverter
    fun fromStringMap(map: Map<String, Float>) = Json.encodeToString(map)
}