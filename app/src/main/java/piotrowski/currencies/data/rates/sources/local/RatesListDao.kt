package piotrowski.currencies.data.rates.sources.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import piotrowski.currencies.data.rates.models.RatesList

@Dao
interface RatesListDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdate(ratesList: RatesList)

    @Query("SELECT * FROM rates_list_table WHERE baseCurrency = :isoCode")
    fun getByIsoCode(isoCode: String): RatesList?
}