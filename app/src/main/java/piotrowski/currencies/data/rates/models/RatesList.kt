package piotrowski.currencies.data.rates.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.serialization.Serializable

@Serializable
@Entity(tableName = "rates_list_table")
data class RatesList(
    /**
     * Currency code defined by ISO agency
     * @see https://www.iso.org/iso-4217-currency-codes.html
     */
    @PrimaryKey
    val baseCurrency: String,

    /**
     * Rates to this currency.
     * First is ISO code, second is rate
     */
    val rates: Map<String, Float>
)