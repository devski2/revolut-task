package piotrowski.currencies

import android.app.Application
import com.blongho.country_data.World
import kotlinx.serialization.ExperimentalSerializationApi
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import piotrowski.currencies.injection.appComponent

class RatesApp : Application(), KoinComponent {

    @ExperimentalSerializationApi
    override fun onCreate() {
        super.onCreate()
        initKoin()
        World.init(applicationContext)
    }

    @ExperimentalSerializationApi
    private fun initKoin() {
        startKoin {
            androidContext(this@RatesApp)
            modules(appComponent)
        }
    }
}
