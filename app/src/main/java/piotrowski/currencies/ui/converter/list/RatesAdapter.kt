package piotrowski.currencies.ui.converter.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import piotrowski.currencies.databinding.RateItemBinding
import piotrowski.currencies.ui.converter.ConverterViewModel
import java.math.BigDecimal

class RatesAdapter(private val viewModel: ConverterViewModel) :
    RecyclerView.Adapter<RateItemViewHolder>() {

    private var view: RecyclerView? = null

    private val multiplierWatcher = MultiplierWatcher(viewModel)

    var ratesList: List<RateItem> = emptyList()
    var errorMessage: String? = null

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        view = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        RateItemViewHolder(
            RateItemBinding
                .inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
        )

    override fun onBindViewHolder(holder: RateItemViewHolder, position: Int) {
        with(holder.binding) {
            when (position) {
                0 -> {
                    //Editable header- currently selected currency
                    clickArea.visibility = View.GONE
                    holder.bind(
                        RateItem(
                            viewModel.selectedCurrency, viewModel.multiplier
                        ),
                        errorMessage
                    )
                    with(rateValueEditText) {
                        removeTextChangedListener(multiplierWatcher)
                        setText(viewModel.multiplier.toString())
                        addTextChangedListener(multiplierWatcher)
                        requestFocus()
                    }
                }
                else -> {
                    //Calculated value for others currencies
                    val item = ratesList[position - 1]
                    holder.bind(item, null)
                    rateValueEditText.removeTextChangedListener(multiplierWatcher)
                    with(clickArea) {
                        visibility = View.VISIBLE
                        clickArea.setOnClickListener { selectCurrency(item.isoCode) }
                    }
                }
            }
        }
    }

    private fun selectCurrency(isoCode: String) {
        val clickedItemIndex =
            ratesList.indexOfFirst { it.isoCode == isoCode }
        notifyItemMoved(clickedItemIndex + 1, 0) //+1 cuz header isn't on the list
        notifyItemRangeChanged(0, itemCount)
        view?.layoutManager?.scrollToPosition(0)
        viewModel.selectCurrency(isoCode, ratesList[clickedItemIndex].rate)
    }

    override fun onViewDetachedFromWindow(holder: RateItemViewHolder) {
        with(holder.binding) {
            rateValueEditText.removeTextChangedListener(multiplierWatcher)
            clickArea.setOnClickListener(null)
        }

        super.onViewDetachedFromWindow(holder)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        view = null

        super.onDetachedFromRecyclerView(recyclerView)
    }

    override fun getItemCount() = ratesList.size + 1

    data class RateItem(val isoCode: String, val rate: BigDecimal)
}
