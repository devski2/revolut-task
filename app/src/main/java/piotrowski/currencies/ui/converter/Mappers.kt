package piotrowski.currencies.ui.converter

import android.content.Context
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import piotrowski.currencies.R
import piotrowski.currencies.data.rates.RatesListRepository
import piotrowski.currencies.ui.converter.list.RatesAdapter
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.math.BigDecimal
import java.math.RoundingMode

fun RatesListRepository.Result.mapToUi(multiplier: BigDecimal): RatesListDomainModel =
    when (this) {
        is RatesListRepository.Result.Initializing ->
            RatesListDomainModel(
                ConverterActivity.State.Initializing,
                emptyList()
            )

        is RatesListRepository.Result.Success ->
            RatesListDomainModel(
                ConverterActivity.State.FreshData,
                data.rates.map {
                    RatesAdapter.RateItem(
                        it.key,
                        (it.value.toBigDecimal() * multiplier).setScale(2, RoundingMode.HALF_EVEN)
                    )
                })

        is RatesListRepository.Result.Cached ->
            RatesListDomainModel(
                ConverterActivity.State.Cached,
                data.rates.map {
                    RatesAdapter.RateItem(
                        it.key,
                        (it.value.toBigDecimal() * multiplier).setScale(2, RoundingMode.HALF_EVEN)
                    )
                })

        is RatesListRepository.Result.Error ->
            RatesListDomainModel(
                ConverterActivity.State.Fail(exception),
                emptyList()
            )
    }

private var namesTable: CcyTbl? = null

fun String.mapToCurrencyName(context: Context): String {
    if (namesTable == null) {
        val inputStream = context.resources.openRawResource(R.raw.currency_names);
        val jsonString: String = inputStreamToString(inputStream)
        namesTable = Json { ignoreUnknownKeys = true }.decodeFromString(jsonString)
    }

    return namesTable?.list?.first { it.Ccy == this }!!.CcyNm
}


fun inputStreamToString(inputStream: InputStream?): String {
    val sb = StringBuilder()
    var line: String?
    val br = BufferedReader(InputStreamReader(inputStream))
    while (br.readLine().also { line = it } != null) {
        sb.append(line)
    }
    br.close()
    return sb.toString()
}

@Serializable
data class CcyTbl(val list: List<CcyNtry>)

@Serializable
data class CcyNtry(val Ccy: String = "", val CcyNm: String = "")
