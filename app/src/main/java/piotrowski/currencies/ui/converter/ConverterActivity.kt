package piotrowski.currencies.ui.converter

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.androidx.viewmodel.ext.android.viewModel
import piotrowski.currencies.R
import piotrowski.currencies.databinding.ActivityMainBinding
import piotrowski.currencies.ui.converter.list.RatesAdapter
import java.io.IOException

class ConverterActivity : AppCompatActivity() {

    sealed class State {
        object Initializing : State()
        object FreshData : State()
        object Cached : State()
        data class Fail(val exception: Exception) : State()
    }

    private val binding: ActivityMainBinding
            by lazy { DataBindingUtil.setContentView(this, R.layout.activity_main) }
    private val viewModel: ConverterViewModel
            by viewModel()
    private val ratesAdapter: RatesAdapter
            by lazy { RatesAdapter(viewModel) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(binding.currencyList) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ConverterActivity)
            adapter = ratesAdapter
        }
    }

    override fun onResume() {
        super.onResume()

        viewModel.currenciesList.observe(this) { list -> updateView(list.mapToUi(viewModel.multiplier)) }
        viewModel.startStreamingRates()
    }

    override fun onPause() {
        viewModel.stopStreamingRates()

        super.onPause()
    }

    private fun updateView(data: RatesListDomainModel) {
        when (data.state) {
            is State.Initializing -> {
                binding.progressView.visibility = View.VISIBLE
                with(ratesAdapter) {
                    ratesList = emptyList()
                    notifyDataSetChanged()
                }
            }
            is State.Cached -> {
                if (ratesAdapter.ratesList == data.ratesList) return

                binding.progressView.visibility = View.GONE
                with(ratesAdapter) {
                    val dataSetChanged = ratesList.size != data.ratesList.size
                    ratesList = data.ratesList
                    if (dataSetChanged)
                        notifyDataSetChanged()
                    else
                        notifyItemRangeChanged(0, itemCount - 1)
                }
            }
            is State.FreshData -> {
                binding.progressView.visibility = View.GONE
                with(ratesAdapter) {
                    if (errorMessage != null) {
                        errorMessage = null
                        notifyItemChanged(0)
                    }
                    val dataSetChanged = ratesList.size != data.ratesList.size
                    ratesList = data.ratesList
                    if (dataSetChanged)
                        notifyDataSetChanged()
                    else
                        notifyItemRangeChanged(1, itemCount - 1)
                }
            }
            is State.Fail -> {
                with(ratesAdapter) {
                    val errorString = when (data.state.exception) {
                        is IOException -> getString(R.string.check_connection)
                        else -> getString(R.string.unexpected_error_occurs)
                    }

                    if (errorMessage != errorString) {
                        errorMessage = errorString
                        notifyItemChanged(0)
                    }
                }
            }
        }
    }
}

data class RatesListDomainModel(
    val state: ConverterActivity.State,
    val ratesList: List<RatesAdapter.RateItem>
)
