package piotrowski.currencies.ui.converter.list

import android.text.Editable
import android.text.TextWatcher
import piotrowski.currencies.ui.converter.ConverterViewModel
import java.math.RoundingMode

class MultiplierWatcher(private val viewModel: ConverterViewModel) : TextWatcher {
    //no-ops
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

    //no-ops
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

    override fun afterTextChanged(value: Editable?) {
        viewModel.multiplier = if (value.toString().isEmpty()) {
            0.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN)
        } else {
            value.toString().toBigDecimal().setScale(2, RoundingMode.HALF_EVEN)
        }
    }
}