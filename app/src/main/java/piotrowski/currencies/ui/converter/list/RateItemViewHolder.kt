package piotrowski.currencies.ui.converter.list

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.blongho.country_data.World
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import piotrowski.currencies.R
import piotrowski.currencies.databinding.RateItemBinding
import piotrowski.currencies.ui.converter.mapToCurrencyName

class RateItemViewHolder(val binding: RateItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: RatesAdapter.RateItem, errorMessage: String?) {
        with(binding) {
            isoCodeText.text = item.isoCode
            Glide.with(root.context)
                .load(
                    if (item.isoCode == "EUR") {
                        R.drawable.flag_of_europ
                    } else {
                        World.getFlagOf(item.isoCode.dropLast(1))
                    }
                )
                .apply(RequestOptions.circleCropTransform())
                .into(flagImg)
            currencyNameText.text =
                item.isoCode.mapToCurrencyName(root.context)
            with(rateValueEditText) {
                setText(item.rate.toString())
            }
            with(extraInfo) {
                visibility = View.GONE
                errorMessage?.let {
                    text = it
                    visibility = View.VISIBLE
                }
            }
        }
    }
}