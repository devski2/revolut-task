package piotrowski.currencies.ui.converter

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect
import piotrowski.currencies.data.rates.RatesListRepository
import piotrowski.currencies.data.rates.RatesListRepository.Result
import piotrowski.currencies.ui.base.BaseViewModel
import java.math.BigDecimal
import java.math.RoundingMode

class ConverterViewModel(
    private val ratesListRepository: RatesListRepository,
    appContext: Application
) : BaseViewModel(appContext) {

    var selectedCurrency: String
        private set
    var multiplier: BigDecimal = BigDecimal(1).setScale(2, RoundingMode.HALF_EVEN)
        set(value) {
            field = value.setScale(2, RoundingMode.HALF_EVEN)
            if (_currenciesList.value is Result.Cached || _currenciesList.value is Result.Success)
                _currenciesList.postValue(currenciesList.value) //emit refresh
        }

    private val _currenciesList: MutableLiveData<Result> = MutableLiveData(Result.Initializing)
    val currenciesList: LiveData<Result>
        get() = _currenciesList

    private var ratesUpdateJob: Job? = null

    init {
        selectedCurrency = DEFAULT_CURRENCY_CODE
    }

    fun selectCurrency(isoCode: String, multiplier: BigDecimal) {
        selectedCurrency = isoCode
        this.multiplier = multiplier
    }

    fun startStreamingRates() {
        ratesUpdateJob = viewModelScope.launch {
            withContext(IO) {
                while (isActive) {
                    ratesListRepository
                        .getByIsoCode(selectedCurrency)
                        .collect { _currenciesList.postValue(it) }
                    delay(REFRESH_FREQUENCY)
                }
            }
        }
    }

    fun stopStreamingRates() {
        ratesUpdateJob?.cancel()
        ratesUpdateJob = null
    }

    companion object {
        private const val DEFAULT_CURRENCY_CODE = "EUR"
        private const val REFRESH_FREQUENCY = 1000L
    }
}
